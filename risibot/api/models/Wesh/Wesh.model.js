const mongoose = require('mongoose');
const { WeshSchema } = require('./Wesh.schema.js');

const VragashModel = mongoose.model('vragash', WeshSchema);
const FlhuetModel = mongoose.model('flhuet', WeshSchema);
const HackerModel = mongoose.model('hacker', WeshSchema);
const PierreModel = mongoose.model('pierre', WeshSchema);

module.exports = {
  vragash: VragashModel,
  flhuet: FlhuetModel,
  hacker: HackerModel,
  pierre: PierreModel,
};
