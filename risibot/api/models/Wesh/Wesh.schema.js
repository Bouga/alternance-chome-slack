const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const WeshSchema = new Schema({
  line: {
    type: String,
    unique: true,
  },
  lines: Array,
  type: String,
});

exports.WeshSchema = WeshSchema;
