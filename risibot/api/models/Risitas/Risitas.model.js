const mongoose = require('mongoose');
const { RisitasSchema } = require('./Risitas.schema.js');

const RisitasModel = mongoose.model('Risitas', RisitasSchema);

exports.RisitasModel = RisitasModel;
