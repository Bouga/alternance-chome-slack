const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const RisitasSchema = new Schema({
  isEphemeral: Boolean,
  isDisabled: Boolean,
});

exports.RisitasSchema = RisitasSchema;
