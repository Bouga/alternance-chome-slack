'use strict';
module.exports = function(app) {
  const risibot = require('../controllers/risibotController');

  app.route('/risitas')
    .post(risibot.parseRequest);
};
