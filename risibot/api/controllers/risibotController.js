'use strict';

const { Risibot } = require('../../Risibot/Risibot.js');

/**
 * @function parseRequest
 * @description Fonction chargée de parser la requête pour diriger vers la bonne fonction du Risibot
 *
 * @param {String} req La requête (tout ce qui suis /risitas)
 * @param {Object} res L'objet de la requête (de type XmlHttpRequest probablement, un truc comme ça) qui va servir à envoyer la réponse
 */
const parseRequest = async function (req, res) {
  const { text, user_id } = req.body;

  // Rejète les requêtes non conforme au format nécessaire
  if (!text || !user_id) {
    res.json({ info_importante_askip: 'Il faut un champ text et un champ user_id... tocard lol' });
    return;
  }

  const args = req.body.text.split(' ');
  const [risiFunction, arg, content] = [Risibot[args[0]], args[1], args.slice(2)];

  if (args.length > 1 && risiFunction) { // si 2 arguments, commande
    res.json(await risiFunction(user_id, arg, content));
  } else { // si 1 argument,
    const resStickers = await Risibot.sendStickers(user_id, text);
    res.json(resStickers);
  }
};

module.exports = {
  parseRequest,
};
