const models = require('./../../api/models/Wesh/Wesh.model.js');
const data = require('./data.json');

/**
 * @function lineObj
 * @description Fonction de mapping des phrases de la DB vers un objet du model Wesh (suivant le schéma WeshSchema)
 *
 * @param {{ String | { type: String, link: String } | { lines: Array<String> } }} l L'objet "ligne" de la base
 *
 * @return {{ lines: Array<String>, line: String | { type: String, link: String } }} Objet "ligne" pouvant faire un block slack (voir wesh.js)
 */
const lineObj = (l) => {
  const lines = l.lines;
  if (lines) {
    return ({ lines });
  }

  return l.type === 'image'
    ? ({ line: l.link, type: 'image' })
    : ({ line: l })
}

/**
 * @function migrate
 * @description Première fonction pour peupler la base avec les phrases
 */
function migrate() {
  Object.keys(models).forEach(async (modelName) => {
    const modelData = data[modelName].map(lineObj);
    const model = models[modelName];
    try {
      await model.insertMany(modelData);
    } catch (e) {
      console.log(e);
      console.log(`Duplicate line in model "${modelName}", probably ignore...`);
    }
  });
}

module.exports.migrate = migrate;
