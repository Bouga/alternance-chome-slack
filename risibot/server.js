try {
  process.env = {
    ...process.env,
    ...require(`./env.${process.env.NODE_ENV}.js`),
  }
} catch (e) {
  throw e;
}

const { connectDB } = require('./connectDB.js');
connectDB();

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require('./api/routes/risibotRoutes.js');
routes(app);

app.listen(port, () => {
  console.log('Risibot RESTful API server started on: ' + port);
});
