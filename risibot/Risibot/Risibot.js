const { sendStickers } = require('./sendStickers.js');
const { wesh } = require('./wesh.js');
const { config } = require('./config.js');
const { save } = require('./save.js');

const Risibot = {
  sendStickers,
  wesh,
  config,
  save,
}

module.exports.Risibot = Risibot;
