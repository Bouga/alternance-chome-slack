'use strinct';

const {
  formatSlackUser,
  getSimpleBlock,
  getEmptyBlock,
  sortStickers,
  risiRequest,
} = require('./utils.js');

const { RisitasModel } = require('./../api/models/Risitas/Risitas.model.js');

/**
 * @function sendStickers
 * @description Fonction qui renvoie un message slack comportant un sticker Risibank (ou le bot répond qu'il a pas trouvé _sueur_)
 *
 * @param {String} userId l'ID du user qui a requêté le Risibot
 * @param {String} query Le text à envoyé à l'api Risibank
 *
 * Note : l'api risibank renvoie rarement quoi que ce soit pour les requêtes de plus de 1 mot.
 *
 * @return {Object} Le message slack
 */
const sendStickers = async function(userId, query) {
  // Objet servant à faire les requêtes risibank
  const risibank = { searchStickers: risiRequest };

  // check de la config du Risibot, pour ephemeral ou disabled
  const risiConfig = await RisitasModel.findOne({});

  // on stop si le bot est disabled
  if (risiConfig && risiConfig.isDisabled) {
    return {
      response_type: 'ephemeral',
      text: ':risitas_fuck:',
    };
  }

  // Object de la réponse finale
  const response = {
    response_type: (risiConfig && risiConfig.isEphemeral) ? 'ephemeral' : 'in_channel',
  };

  try {
    // réponse de l'api risibank, avec une fat liste de stickers (ou rien)
    const risibankResponse = (await risibank.searchStickers(query)).stickers;

    // filtre des 30 meilleurs stickers (selon les likes et vues il me semble, voir la fonction sortStickers)
    const risiResponse = sortStickers(risibankResponse).slice(0, 30);

    if (risiResponse.length > 0) {
      // Stickers trouvés, on en renvoie un au pif
      const sticker = risiResponse[Math.floor(Math.random() * risiResponse.length)];
      response.blocks = [getSimpleBlock(sticker.risibank_link, query)];
    } else {
      // Aucune stickers trouvés, on renvoie la réponse préformattée
      const sueurReq = await risibank.searchStickers('sueur');
      const sticker = sueurReq[Math.floor(Math.random() * sueurReq.length)];
      response.blocks = [getEmptyBlock(sticker.risibank_link, formatSlackUser(userId), query)];
    }
    return response;
  } catch (e) {
    // Erreur wtf, là je sais pas trop _shrugs_
    response.text = `OULA, ERREUR INTERNE :soral_surpris: :risitas_peur: :jesus_bras: (erreur : ${e.message})`;
    return response;
  }
};

module.exports.sendStickers = sendStickers;
