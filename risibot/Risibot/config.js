const { formatSlackUser } = require('./utils.js');
const { RisitasModel } = require('./../api/models/Risitas/Risitas.model.js');

const possibleArgs = {
  ephemeral: 'isEphemeral',
  disabled: 'isDisabled',
};

/**
 * @function config
 * @description Fonction de config du Risibot, pour changer son état, l'éteindre/allumer etc...
 * @see https://gitlab.com/Bouga/alternance-chome-slack/-/blob/master/README.md
 *
 * @param {String} userId L'ID du user qui requête le Risibot (pour dire qui change la conf du Risibot)
 * @param {String} arg La config qui doit être modifié (cf. le README)
 *
 * @return {Object} Un message slack pour dire qui a modifié quelle config.
 */
const config = async function (userId, arg) {
  // si le user n'est pas MOI, stop (liste des admins stockées en base pour plus tard)
  if (userId !== 'UBMNM8N7N') {
    return {
      response_type: 'ephemeral',
      text: 'Désolé sale branle slack, t\'as pas le droit de faire ça :risitas_tant_pis: :risitas_fuck:',
    };
  } else {
    // stop si la commande de config n'existe pas
    if (!Object.keys(possibleArgs).includes(arg)) {
      return {
        response_type: 'ephemeral',
        text: `Désolé chôme slack, la commande de config "${arg}" n'existe pas :risitas_mouchoir:`,
      };
    }

    const risiConfig = await RisitasModel.findOne({});

    if (!risiConfig) { // si aucune config n'existe, mettre celle par défaut
      RisitasModel.create({
        isEphemeral: true,
        isDisabled: false,
      }, (err, res) => {
        console.log(err, res);
      });
    } else { // si une config existe, inverser le paramètre indiquée
      const updated = {};
      updated[possibleArgs[arg]] = !risiConfig[possibleArgs[arg]];
      const res = await RisitasModel.updateMany({ }, updated);
      console.log(res);
    }

    return {
      response_type: 'in_channel',
      text: `${formatSlackUser(userId)} vient de set le mode ${arg} du bot à ${!risiConfig[possibleArgs[arg]]} :risitas_parfait:`,
    }
  }
};

module.exports.config = config;
