const {
  getTextBlock,
  getSimpleBlock,
  formatSlackUser,
} = require('./utils.js');

const { RisitasModel } = require('./../api/models/Risitas/Risitas.model.js');
const models = require('./../api/models/Wesh/Wesh.model.js');

/**
 * @function wesh
 * @description Fonction gérant les réponses du Risibot vis-à-vis des "wesh vragash" etc...
 *
 * @param {String} userId L'ID du user qui requête le Risibot (au cas où ça requête n'importe quoi)
 * @param {String} arg Le "wesh" voulu (qui correspond à un model Mongoose)
 *
 * @return {Object} Un message slack avec la phrase random (parmis les phrases enregistrées pour ce "wesh <truc")
 */
const wesh = async function(uesrId, arg) {
  // check de la config du Risibot, pour ephemeral ou disabled
  const risiConfig = await RisitasModel.findOne({});

  // on stop si le bot est disabled
  if (risiConfig && risiConfig.isDisabled) {
    return {
      response_type: 'ephemeral',
      text: ':risitas_fuck:',
    };
  }

  // Object de la réponse finale
  const response = {
    response_type: (risiConfig && risiConfig.isEphemeral) ? 'ephemeral' : 'in_channel',
  };

  // Model utilisé (aka nom de la collection correspondant à la commande call)
  const model = models[arg];

  // Si la commande "wesh TRUC" existe pas
  if (!model) {
    response.blocks = [getTextBlock(`Le "wesh ${arg}" n'existe pas sale branle slack de ${formatSlackUser(userId)}`)];
    return response;
  }

  // liste de toute les phrases dispo pour "wesh <arg>"
  const lines = await model.find({});

  // Si aucune phrase n'est enregistrée pour la commande call
  if (lines.length === 0) {
    response.blocks = [getTextBlock(`Pas de phrases enregistrées pour la commande "wesh ${arg}" :risitas_triste:`)];
    return response;
  }

  // phrase au pif parmis celles disponibles
  const randomLine = lines[Math.floor(Math.random() * lines.length)];

  // cas d'une phrase trop longue pour être stockée dans le mongo
  const text = randomLine.lines && randomLine.lines.length
    ? randomLine.lines.join(' ')
    : randomLine.line;

  response.blocks = [randomLine.type === 'image' ? getSimpleBlock(text, `wesh ${arg}`) : getTextBlock(text)];
  return response;
};

module.exports.wesh = wesh;
