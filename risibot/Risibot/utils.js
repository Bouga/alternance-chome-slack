const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args));

/**
 * @function getSimpleBlock
 * @description Renvoie un "block" slack contenant l'url du sticker Risibank
 *
 * @param {String} url Url du sticker Risibank
 * @param {String} query Query originale, dans le cas d'erreur (aka Alt pour une balise img)
 *
 * @returns {Object} Le block slack
 */
module.exports.getSimpleBlock = function (url, query) {
  return {
    type: 'image',
    image_url: url,
    alt_text: query
  };
}

/**
 * @function getTextBlock
 * @description Renvoie un "block" slack contenant seulement du texte
 *
 * @param {String} query Texte àmettre dans le block
 *
 * @return {Object} Le block slack
 */
module.exports.getTextBlock = function (query) {
  return {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: query,
    },
  };
}

/**
 * @function getEmptyBlock
 * @description Renvoie un "block" slack contenant le message indiquant qu'il n'y a eu aucun résultat de la part de Risibank
 *
 * @param {String} url Url du sticker "sueur" sélectionné
 * @param {String} user L'id du user qui a envoyé le message slack ayant trigger cet appel
 * @param {string} query Query originale, dans le cas d'erreur (aka Alt pour une balise img)
*/
module.exports.getEmptyBlock = function (url, user, query) {
  return {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: `Désolé wesh ${user} slack branle chome, il y a pas de sticker "${query}" :risitas_tant_pis:`,
    },
    accessory: {
      type: 'image',
      image_url: url,
      alt_text: query,
    },
  };
}

/**
 * @function stortStickers
 * @description Trie les stickers par nombre de likes/vues (score basique)
 *
 * @param {Array} stickers Liste de stickers à trier
 *
 * @returns {Array} Liste de stickers triés
 */
module.exports.sortStickers = function (stickers) {
  return stickers.sort((a, b) => {
    const scoreA = a.likes + a.views;
    const scoreB = b.likes + b.views;

    return scoreA < scoreB ? 1 : -1;
  });
}

/**
 * @function formatSlackUser
 * @description Format un userId pour être complient avec le format d'userId slack
 *
 * @param {String} userId
 *
 * @returns {String} Le userId formatté
 */
module.exports.formatSlackUser = function (userId) {
  return `<@${userId}>`;
}

module.exports.risiRequest = async function (query) {
  if (fetch) {
    const response = await fetch(`https://api.risibank.fr/api/v0/search?search=${query}`, { method: 'GET' });
    console.log(response);
    return await response.json();
  } else {
    throw new Error('Fetch est pas dispo, bah dommage :risitas_mouchoir:');
  }
}
