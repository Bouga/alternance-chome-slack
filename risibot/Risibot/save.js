const {
  getTextBlock,
  getSimpleBlock,
  formatSlackUser,
} = require('./utils.js');

const { RisitasModel } = require('./../api/models/Risitas/Risitas.model.js');
const models = require('./../api/models/Wesh/Wesh.model.js');

/**
 * @function slicePhrase
 * @description Fonction qui découpe un très long texte en liste de string de taille N.
 *
 * @param {String} phrase Le texte à couper.
 * @param {Number} n La taille des chunks de phrase.
 *
 * @return {Array<String>} La phrase coupée.
 */
const slicePhrase = (phrase, n) => {
  const res = [];
  while (phrase.length > 0) {
    res.push(phrase.slice(0, n));
    phrase = phrase.slice(n);
  }
  return res;
}

/**
 * @function errorResponse
 * @description Petite fonction pour renvoyer un message en ephemeral quand la commande est pas bonne.
 *
 * @param {String} text
 *
 * @return {Object} Un message slack en ephemeral
 */
const errorResponse = (text) => ({
  response_type: 'ephemeral',
  text,
});

/**
 * @function save
 * @description Fonction qui permet d'ajouter des phrases au bot (pour les commandes /risitas wesh <truc>)
 *
 * @param {String} userId
 * @param {String} arg Personne pour qui la phrase va être enregistrée (vragash, flhuet, etc...)
 * @param {string} content Phrase en elle-même
 */
const save = async (userId, arg, content) => {
  // check de la config du Risibot, pour ephemeral ou disabled
  const risiConfig = await RisitasModel.findOne({});

  // on stop si le bot est disabled
  if (risiConfig && risiConfig.isDisabled) {
    return errorResponse(':risitas_fuck:');
  }

  if (content.length === 0) {
    return errorResponse('Je veux bien ajouter une phrase, mais si tu me donnes pas de phrase, je fais comment ? :risitas_perplexe: :risitas_tant_pis:');
  }

  // Object de la réponse finale
  const response = {
    response_type: (risiConfig && risiConfig.isEphemeral) ? 'ephemeral' : 'in_channel',
  };

  // Model utilisé (aka nom de la collection correspondant à la commande call)
  const model = models[arg];

  // Si la commande "wesh TRUC" existe pas
  if (!model) {
    response.blocks = [getTextBlock(`Le "save ${arg}" n'existe pas sale branle slack de ${formatSlackUser(userId)}`)];
    return response;
  }

  const phrase = content.join(' ');

  const sameLine = await model.find({ line: phrase }).exec();
  if (sameLine.length > 0) {
    return errorResponse('La phrase a déjà été sauvegardée, tu voudrais pas polluer la base de données par hazard ??? :risitas_perplexe: :risitas_gilbert_lunettes:');
  }

  const obj = phrase.length < 550
    ? { line: phrase }
    : { lines: slicePhrase(phrase, 550) };

  try {
    await model.create(obj);
    response.text = `${formatSlackUser(userId)} vient d'ajouter une phrase dans \`wesh ${arg}\`, pour la voir, vous avez qu'à spam le bot :risitas_zoom: :risitas_ayaa:`;
    return response;
  } catch (e) {
    return errorResponse('La sauvegarde de la phrase a foiré :risitas_sueur2:, ce message aidera peut-être : ' + e.message);
  }
};

module.exports.save = save;
