const mongoose = require('mongoose');
const { migrate } = require('./scripts/2021-03-11/index.js');

exports.connectDB = () => {
  mongoose.connect(
    process.env.MONGO_URL,
    {
      useNewUrlParser: true,
    },
  );

  const db = mongoose.connection;
  db.on('error', console.error.bind(console, 'Erreur lors de la connexion'));
  db.once('open', function () {
    console.log('DB connected successfully');
    // migrate();
  });
};
