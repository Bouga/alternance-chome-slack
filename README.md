# Alternance Chôme Slack

Le slackbot du slack alternance branle slack chôme bastos wesh vragash

## Liste de commande

### `/risitas sticker`

Commande classique pour avoir des stickers de Risibank.

```bash
# renvoie un stickers eussou
/risitas eussou

# renvoie un stickers "gilet" (gilet jaune probablement)
/risitas gilet

# renvoie un message disant que le bot a rien trouvé parce que c'est sûrement ce qu'il va se passer
/risitas srhdgjlolmdrtuveuxquoi
```

### `/risitas config param`

Switch le mode `param` du bot à `true` ou `false`.

Paramètre du bot :
- `ephemeral` (réponses du bot uniquement visibles par la personne qui l'invoque)
- `disabled` (réponses du bot désactivées)

```bash
# Passe le mode ephemeral du bot à true
/risitas config ephemeral
# ... et la repasse à true
/risitas config ephemeral

# Pareil pour le mode disabled
/risitas config disabled
/risitas config disabled
```

### `/risitas wesh param`

Commande pour avoir une phrase aléatoire selon `param` (un type de réponse).

Type de réponse possible :
- vragash
- hacker
- flhuet
- pierre

```bash
# Renvoie une phrase de "wesh vragash" (si au moins une est enregistrée, sinon un petit message en ephemeral)
/risitas wesh vragash
```

### `/risitas save param Une phrase de K-litaient`

Commande pour ajouter une phrase au bot à `param` (un type de réponse).

Type de réponse pouvant recevoir des phrases :
- vragash
- hacker
- flhuet
- pierre

```bash
# Ajoute une phrase de "wesh vragash"
/risitas save vragash Tu va voir toi à la rentrée, c'est pas je dis bonjour

/risitas save pierre Alors en fait hier j'ai fait un rêve et personne va lire donc je vais m'arrêter là.
```

### `/risitas delete param`

> TODO : implémenter la suppression de phrase, qui est important mais n'existe pas aujourd'hui _shrugs_.
